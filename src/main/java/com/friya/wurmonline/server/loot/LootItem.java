package com.friya.wurmonline.server.loot;

import com.wurmonline.server.FailedException;
import com.wurmonline.server.Server;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemFactory;
import com.wurmonline.server.items.ItemSpellEffects;
import com.wurmonline.server.items.NoSuchTemplateException;
import com.wurmonline.server.items.WurmColor;
import com.wurmonline.server.spells.SpellEffect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LootItem {
   private static Logger logger = Logger.getLogger(LootItem.class.getName());
   private int lootId;
   private int[] wurmItemIds;
   private String name = "";
   private byte material = -1;
   private float startQl = 10.0F;
   private float endQl = 40.0F;
   private boolean canBeRare = true;
   private double dropChance = 1.0;
   private String creator = "";
   private int auxData = 0;
   private long decayTime = 0L;
   private float damage = 0.0F;
   private String customMethod;
   private String customArgument;
   private int cloneCount = 1;
   private int cloneCountRandom = 0;
   private String enchants = "";
   private int enchantStrength = 0;
   private int enchantStrengthRandom = 0;
   private int realTemplate = 0;
   private int colorR = -1;
   private int colorG = -1;
   private int colorB = -1;
   private int colorRrandom = 0;
   private int colorGrandom = 0;
   private int colorBrandom = 0;
   private int weight = -1;
   private int weightRandom = 0;
   private int forceRare = 0;
   private List<Byte> enchantIds = new ArrayList();

   static LootItem getFromDB(ResultSet rs) throws SQLException {
      LootItem li = new LootItem(rs.getInt("id"), rs.getString("itemids"), rs.getString("name"), rs.getByte("material"), rs.getFloat("startql"), rs.getFloat("endql"), rs.getByte("canberare"), rs.getDouble("dropchance"), rs.getString("creator"), rs.getInt("auxdata"), rs.getLong("decaytime"), rs.getFloat("damage"), rs.getString("custommethod"), rs.getString("customargument"), rs.getInt("clonecount"), rs.getInt("clonecountrandom"), rs.getString("enchants"), rs.getInt("enchantstrength"), rs.getInt("enchantstrengthrandom"), rs.getInt("realtemplate"), rs.getInt("colorr"), rs.getInt("colorg"), rs.getInt("colorb"), rs.getInt("colorrrandom"), rs.getInt("colorgrandom"), rs.getInt("colorbrandom"), rs.getInt("weight"), rs.getInt("weightrandom"), rs.getInt("forcerare"));
      return li;
   }

   int save(Connection con) {
      boolean update = false;
      String sql;
      if (this.lootId > 0) {
         sql = "UPDATE FriyaLootItems SET \t\titemids=?, name=?, material=?, startql=?, endql=?, canberare=?, dropchance=?, creator=?, auxdata=?, decaytime=?, damage=?, custommethod=?, \t\tcustomargument=?, clonecount=?, clonecountrandom=?, enchants=?, enchantstrength=?, enchantstrengthrandom=?, realtemplate=?, \t\tcolorr=?, colorg=?, colorb=?, colorrrandom=?, colorgrandom=?, colorbrandom=?, weight=?, weightrandom=?, forcerare=? WHERE id = " + this.getId();
         update = true;
      } else {
         sql = "INSERT INTO FriyaLootItems(\t\titemids, name, material, startql, endql, canberare, dropchance, creator, auxdata, decaytime, damage, custommethod, customargument, \t\tclonecount, clonecountrandom, enchants, enchantstrength, enchantstrengthrandom, realtemplate,\t\tcolorr, colorg, colorb, colorrrandom, colorgrandom, colorbrandom, weight, weightrandom, forcerare) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      try {
         Object ps;
         if (Mod.sqlLogging) {
            ps = new LoggableStatement(con, sql);
         } else {
            ps = con.prepareStatement(sql, 1);
         }

         int i = 1;
         ((PreparedStatement)ps).setString(i++, this.getWurmItemIds(true));
         ((PreparedStatement)ps).setString(i++, this.getName());
         ((PreparedStatement)ps).setByte(i++, this.material);
         ((PreparedStatement)ps).setFloat(i++, this.startQl);
         ((PreparedStatement)ps).setFloat(i++, this.endQl);
         ((PreparedStatement)ps).setByte(i++, (byte)(this.canBeRare ? 1 : 0));
         ((PreparedStatement)ps).setDouble(i++, this.dropChance);
         ((PreparedStatement)ps).setString(i++, this.creator);
         ((PreparedStatement)ps).setInt(i++, this.auxData);
         ((PreparedStatement)ps).setLong(i++, this.decayTime);
         ((PreparedStatement)ps).setFloat(i++, this.damage);
         ((PreparedStatement)ps).setString(i++, this.customMethod);
         ((PreparedStatement)ps).setString(i++, this.customArgument);
         ((PreparedStatement)ps).setInt(i++, this.cloneCount);
         ((PreparedStatement)ps).setInt(i++, this.cloneCountRandom);
         ((PreparedStatement)ps).setString(i++, this.enchants);
         ((PreparedStatement)ps).setInt(i++, this.enchantStrength);
         ((PreparedStatement)ps).setInt(i++, this.enchantStrengthRandom);
         ((PreparedStatement)ps).setInt(i++, this.realTemplate);
         ((PreparedStatement)ps).setInt(i++, this.colorR);
         ((PreparedStatement)ps).setInt(i++, this.colorG);
         ((PreparedStatement)ps).setInt(i++, this.colorB);
         ((PreparedStatement)ps).setInt(i++, this.colorRrandom);
         ((PreparedStatement)ps).setInt(i++, this.colorGrandom);
         ((PreparedStatement)ps).setInt(i++, this.colorBrandom);
         ((PreparedStatement)ps).setInt(i++, this.weight);
         ((PreparedStatement)ps).setInt(i++, this.weightRandom);
         ((PreparedStatement)ps).setInt(i++, this.forceRare);
         if (Mod.sqlLogging) {
            logger.log(Level.INFO, "Executing: " + ((LoggableStatement)ps).getQueryString());
         }

         if (update) {
            ((PreparedStatement)ps).executeUpdate();
         } else {
            ((PreparedStatement)ps).execute();
         }

         ResultSet rs = ((PreparedStatement)ps).getGeneratedKeys();
         if (rs != null) {
            rs.next();
            this.lootId = rs.getInt(1);
            logger.log(Level.FINE, "Inserted item as: " + this.lootId);
         } else {
            logger.log(Level.WARNING, "no resultset back from getGeneratedKeys(), probably means nothing was created!");
         }

         ((PreparedStatement)ps).close();
      } catch (SQLException var7) {
         logger.log(Level.SEVERE, "Failed to save LootItem", var7);
         throw new RuntimeException(var7);
      }

      return this.lootId;
   }

   public LootItem(int id, String wurmItemIds, String name, byte material, float startQl, float endQl, byte canBeRare, double dropChance, String creator, int auxData, long decayTime, float damage, String customMethod, String customArgument, int cloneCount, int cloneCountRandom, String enchants, int enchantStrength, int enchantStrengthRandom, int realTemplate, int colorR, int colorG, int colorB, int colorRrandom, int colorGrandom, int colorBrandom, int weight, int weightRandom, int forceRare) {
      this.lootId = id;
      this.wurmItemIds = this.parseItemWurmIds(wurmItemIds);
      this.name = name;
      this.material = material;
      this.startQl = startQl;
      this.endQl = endQl;
      this.canBeRare = canBeRare == 1;
      this.setDropChance(dropChance);
      this.creator = creator;
      this.auxData = auxData;
      this.decayTime = decayTime;
      this.damage = damage;
      this.customMethod = customMethod;
      this.customArgument = customArgument;
      this.cloneCount = cloneCount;
      this.cloneCountRandom = cloneCountRandom;
      this.enchants = enchants;
      this.enchantStrength = enchantStrength;
      this.enchantStrengthRandom = enchantStrengthRandom;
      this.realTemplate = realTemplate;
      this.colorR = colorR;
      this.colorG = colorG;
      this.colorB = colorB;
      this.colorRrandom = colorRrandom;
      this.colorGrandom = colorGrandom;
      this.colorBrandom = colorBrandom;
      this.weight = weight;
      this.weightRandom = weightRandom;
      this.forceRare = forceRare;
   }

   public LootItem(String wurmItemIds, double dropChance) {
      this.wurmItemIds = this.parseItemWurmIds(wurmItemIds);
      this.dropChance = dropChance;
   }

   public LootItem(String wurmItemIds, byte material, double dropChance, String creator) {
      this.wurmItemIds = this.parseItemWurmIds(wurmItemIds);
      this.material = material;
      this.dropChance = dropChance;
      this.creator = creator;
   }

   private int[] parseItemWurmIds(String wurmItemIds) {
      String[] tmp = wurmItemIds.split(",");
      int[] ret = new int[tmp.length];

      for(int x = 0; x < tmp.length; ++x) {
         ret[x] = Integer.parseInt(tmp[x].trim());
      }

      return ret;
   }

   public int[] getWurmItemIds() {
      return this.wurmItemIds;
   }

   public String getWurmItemIds(boolean asString) {
      if (asString) {
         return Arrays.toString(this.getWurmItemIds()).replace(" ", "").replace("[", "").replace("]", "");
      } else {
         throw new RuntimeException("Use the other getWurmItemIds()");
      }
   }

   public int getId() {
      return this.lootId;
   }

   public Item create(int wurmItemId) {
      Item item = null;

      try {
         float ql;
         if (this.endQl != this.startQl && !(this.startQl > this.endQl)) {
            ql = this.startQl + (float)(this.endQl < this.startQl ? 0 : Server.rand.nextInt((int)(this.endQl - this.startQl)));
         } else {
            ql = this.endQl;
         }

         item = ItemFactory.createItem(wurmItemId, ql, (byte)0, this.creator);
         if (this.getName() != null && this.getName().length() > 0 && this.wurmItemIds.length == 1) {
            item.setName(this.getName());
         }

         if (this.material > 0) {
            item.setMaterial(this.material);
         }

         item.setDamage(this.damage);
         item.setAuxData((byte)this.auxData);
         if (this.realTemplate > 0) {
            item.setRealTemplate(this.realTemplate);
         }
      } catch (NoSuchTemplateException | FailedException var4) {
         logger.log(Level.SEVERE, "Failed to create item with template id: " + wurmItemId);
         var4.printStackTrace();
      }

      this.setColor(item);
      this.setWeight(item);
      this.enchant(item);
      this.handleRarify(item);
      return item;
   }

   private void setWeight(Item item) {
      if (this.weight != -1 || this.weightRandom > 0) {
         int newWeight = this.weight > 0 ? this.weight : item.getWeightGrams();
         if (this.weightRandom > 0) {
            newWeight += Server.rand.nextInt(this.weightRandom);
         }

         item.setWeight(newWeight, false);
      }

   }

   private void setColor(Item item) {
      int newR = 0;
      int newG = 0;
      int newB = 0;
      if (this.colorR != -1 || this.colorB != -1 || this.colorG != -1) {
         if (this.colorR >= 0) {
            newR = this.colorR;
         }

         if (this.colorG >= 0) {
            newG = this.colorG;
         }

         if (this.colorB >= 0) {
            newB = this.colorB;
         }

         if (this.colorRrandom > 0) {
            newR += Server.rand.nextInt(this.colorRrandom);
         }

         if (this.colorGrandom > 0) {
            newG += Server.rand.nextInt(this.colorGrandom);
         }

         if (this.colorBrandom > 0) {
            newB += Server.rand.nextInt(this.colorBrandom);
         }

         if (newR != -1 || newG != -1 && newB != -1) {
            item.setColor(WurmColor.createColor(newR < 0 ? 0 : newR & 255, newG < 0 ? 0 : newG & 255, newB < 0 ? 0 : newB & 255));
         }

      }
   }

   private boolean enchant(Item item) {
      if (this.enchants == null && this.enchantIds.size() == 0) {
         return true;
      } else {
         if (this.enchantIds.size() == 0) {
            String[] enchantArr = this.enchants.trim().split(" ");
            if (enchantArr.length > 0) {
               String[] var6 = enchantArr;
               int var5 = enchantArr.length;

               for(int var4 = 0; var4 < var5; ++var4) {
                  String ench = var6[var4];
                  if (LootSystem.getInstance().enchantAbbreviations.containsKey(ench)) {
                     this.enchantIds.add((Byte)LootSystem.getInstance().enchantAbbreviations.get(ench));
                     Stats.inc("drop.total.enchanted." + ench);
                  } else if (this.isNumeric(ench)) {
                     this.enchantIds.add((byte)Integer.parseInt(ench));
                     Stats.inc("drop.total.enchanted." + ench);
                  } else {
                     logger.warning("Enchant " + ench + " for LootItem " + this.lootId + " is invalid");
                  }
               }
            }
         }

         Iterator var9 = this.enchantIds.iterator();

         while(var9.hasNext()) {
            byte ench = (Byte)var9.next();
            double power = (double)this.enchantStrength;
            if (this.enchantStrengthRandom > 0) {
               power += (double)Server.rand.nextInt(this.enchantStrengthRandom);
            }

            logger.fine("Enchanting " + item.getName() + " with " + ench + " [" + power + "]");
            this.cast(ench, power, item);
         }

         return true;
      }
   }

   private void cast(byte enchant, double power, Item target) {
      ItemSpellEffects effs = target.getSpellEffects();
      if (effs == null) {
         effs = new ItemSpellEffects(target.getWurmId());
      }

      SpellEffect eff = new SpellEffect(target.getWurmId(), enchant, (float)power, 20000000);
      effs.addSpellEffect(eff);
   }

   private boolean isNumeric(String str) {
      try {
         double var2 = Double.parseDouble(str);
         return true;
      } catch (NumberFormatException var4) {
         return false;
      }
   }

   void handleRarify(Item i) {
      if (this.forceRare > 0) {
         i.setRarity((byte)this.forceRare);
      } else {
         boolean isBoneCollar = i.getTemplateId() == 867;
         if (this.canBeRare || isBoneCollar) {
            byte rrarity = (byte)(Server.rand.nextInt(100) != 0 && !isBoneCollar ? 0 : 1);
            if (rrarity > 0) {
               rrarity = (byte)(Server.rand.nextInt(100) == 0 && isBoneCollar ? 2 : 1);
            }

            if (rrarity > 1) {
               rrarity = (byte)(Server.rand.nextInt(100) == 0 && isBoneCollar ? 3 : 2);
            }

            i.setRarity(rrarity);
         }
      }
   }

   double getDropChance() {
      return this.dropChance;
   }

   private void setDropChance(double dropChance) {
      this.dropChance = dropChance;
   }

   int getCloneCount() {
      return this.cloneCount;
   }

   int getCloneCountRandom() {
      return this.cloneCountRandom;
   }

   String getName() {
      return this.name;
   }
}
