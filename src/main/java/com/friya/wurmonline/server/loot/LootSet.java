package com.friya.wurmonline.server.loot;

import com.wurmonline.server.Server;
import com.wurmonline.server.items.Item;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;

public class LootSet {
   private static Logger logger = Logger.getLogger(LootSet.class.getName());
   int maxNumLoot = 1;
   HashMap<Integer, LootTable> lootTables = new HashMap();
   private List<LootRule> lootRules = new ArrayList();

   public LootSet() {
   }

   LootSet(HashSet<Integer> tableIds, List<LootRule> rules, int maxNumLoot) {
      Connection con = ModSupportDb.getModSupportDb();
      this.maxNumLoot = maxNumLoot;
      this.lootRules = rules;
      LootTable lt = null;
      LootItem li = null;
      Iterator var10 = tableIds.iterator();

      while(var10.hasNext()) {
         int id = (Integer)var10.next();
         if (LootSystem.lootTables.containsKey(id)) {
            logger.log(Level.FINE, "Cache hit on lootTable: " + id);
            lt = (LootTable)LootSystem.lootTables.get(id);
         } else {
            logger.log(Level.FINE, "Cache MISS on lootTable: " + id);

            try {
               String sql = "SELECT i.* FROM FriyaLootTables AS t INNER JOIN FriyaLootItems AS i ON (t.lootid = i.id) WHERE tableid = ?";
               PreparedStatement ps = con.prepareStatement(sql);
               ps.setInt(1, id);
               ResultSet rs = ps.executeQuery();
               lt = new LootTable(id);

               for(li = null; rs.next(); lt.addLootItem(li)) {
                  if (LootSystem.lootItems.containsKey(rs.getInt(1))) {
                     logger.log(Level.FINE, "Cache hit on lootItem: " + rs.getInt(1) + " (of lootTable: " + id + ")");
                     li = (LootItem)LootSystem.lootItems.get(rs.getInt(1));
                  } else {
                     logger.log(Level.FINE, "Cache MISS on lootItem: " + rs.getInt(1) + " (of lootTable: " + id + ")");
                     li = LootItem.getFromDB(rs);
                     LootSystem.lootItems.put(rs.getInt(1), li);
                  }
               }

               LootSystem.lootTables.put(id, lt);
            } catch (SQLException var12) {
               logger.log(Level.SEVERE, "Failed to get loot-table", var12);
               throw new RuntimeException(var12);
            }
         }

         this.lootTables.put(id, lt);
         Stats.inc("table.triggered." + lt.getTableId());
      }

   }

   private int[] shuffleArray(int[] array) {
      Random random = new Random();

      for(int i = array.length - 1; i > 0; --i) {
         int index = random.nextInt(i + 1);
         int temp = array[index];
         array[index] = array[i];
         array[i] = temp;
      }

      return array;
   }

   public Item[] getLoot() {
      boolean onlyGuaranteedDrops = false;
      ArrayList<Item> result = new ArrayList();
      HashMap<Integer, LootItem> lootCandidates = new HashMap();
      HashMap<Integer, LootItem> lootDecisions = new HashMap();
      logger.log(Level.FINE, "Found " + this.lootTables.size() + " matching loot-tables!");
      if (this.maxNumLoot == 0) {
         logger.log(Level.INFO, "LootSet had maxNumLoot set to 0, will only return items with 100% drop chance for this one");
         onlyGuaranteedDrops = true;
      }

      Iterator var6 = this.lootTables.values().iterator();

      while(var6.hasNext()) {
         LootTable lt = (LootTable)var6.next();
         lootCandidates.putAll(lt.getLootItemCandidatesByWurmId());
      }

      Object[] tmp = lootCandidates.keySet().toArray();
      int[] randomizedCandidates = new int[tmp.length];

      int decision;
      for(decision = 0; decision < tmp.length; ++decision) {
         randomizedCandidates[decision] = (Integer)tmp[decision];
      }

      randomizedCandidates = this.shuffleArray(randomizedCandidates);
      logger.log(Level.FINE, "getLoot(), candidates: " + lootCandidates.toString());
      logger.log(Level.FINE, "getLoot(), candidates in randomized [Fisher–Yates] order: " + Arrays.toString(randomizedCandidates));
      int[] var10 = randomizedCandidates;
      int var9 = randomizedCandidates.length;

      for(int var8 = 0; var8 < var9; ++var8) {
         decision = var10[var8];
         LootItem candidate = (LootItem)lootCandidates.get(decision);
         if (candidate.getDropChance() >= 99.9999008178711) {
            lootDecisions.put(decision, candidate);
            Stats.inc("drop.total.guaranteed");
         } else if (!onlyGuaranteedDrops && lootDecisions.size() < this.maxNumLoot) {
            double rnd = Server.rand.nextDouble() * 100.0;
            logger.log(Level.FINEST, "Rolling for loot-item ID: " + candidate.getId() + "[" + candidate.getWurmItemIds() + "] Drop chance is: " + String.format("%.4f", candidate.getDropChance()) + "% RNG rolled: " + String.format("%.4f", rnd));
            if (rnd <= candidate.getDropChance()) {
               logger.log(Level.FINEST, "RNG says this item should be dropped!");
               lootDecisions.put(decision, candidate);
            } else {
               logger.log(Level.FINEST, "RNG says no drop this time.");
               Stats.inc("drop.missed." + candidate.getId() + "." + candidate.getName());
            }
         }
      }

      logger.log(Level.FINE, "getLoot(), decisions: " + lootDecisions.toString());
      Iterator var16 = lootDecisions.keySet().iterator();

      while(var16.hasNext()) {
         decision = (Integer)var16.next();
         LootItem ld = (LootItem)lootDecisions.get(decision);
         int clones = ld.getCloneCount();
         if (ld.getCloneCountRandom() > 0) {
            clones += Server.rand.nextInt(ld.getCloneCountRandom());
         }

         for(int n = 0; n < clones; ++n) {
            Item item = ld.create(decision);
            if (item != null) {
               result.add(item);
               if (item.getRarity() > 0) {
                  Stats.inc("drop.total.rares");
               }
            }
         }

         Stats.inc("drop.triggered." + ld.getId() + "." + ld.getName());
         Stats.inc("drop.dropped." + ld.getId() + "." + ld.getName(), clones);
         Stats.inc("drop.total", clones);
      }

      logger.log(Level.FINE, "Returning CREATED items, PLACE in world OR DESTROY: " + result.toString());
      return (Item[])result.toArray(new Item[0]);
   }

   List<LootRule> getLootRules() {
      return this.lootRules;
   }
}
