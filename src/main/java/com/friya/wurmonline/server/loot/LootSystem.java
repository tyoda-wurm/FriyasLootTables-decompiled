package com.friya.wurmonline.server.loot;

import com.wurmonline.mesh.Tiles;
import com.wurmonline.server.Items;
import com.wurmonline.server.Players;
import com.wurmonline.server.Server;
import com.wurmonline.server.WurmCalendar;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemTemplate;
import com.wurmonline.server.items.ItemTemplateFactory;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.spells.Spell;
import com.wurmonline.server.zones.FaithZone;
import com.wurmonline.server.zones.FocusZone;
import com.wurmonline.server.zones.NoSuchZoneException;
import com.wurmonline.server.zones.Zones;
import com.wurmonline.shared.constants.Enchants;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;

public final class LootSystem implements ExcludedItems, Enchants {
   private static LootSystem instance;
   private static Logger logger = Logger.getLogger(LootSystem.class.getName());
   static HashMap<Integer, LootItem> lootItems = new HashMap();
   static HashMap<Integer, LootTable> lootTables = new HashMap();
   HashMap<String, Byte> enchantAbbreviations = new HashMap();
   Spell[] spells;
   private List<BeforeDropListener> beforeDropListeners = new ArrayList();
   static String lootItemInsert = "INSERT INTO FriyaLootItems(id, itemids, name, material, startql, endql, canberare, dropchance, creator, decaytime, damage) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
   static String lootRuleQuery = "SELECT id, loottable, maxloot, rulename FROM FriyaLootRules WHERE \n\t\t(creature = '*' OR creature = ?) \n\t\tAND (age = '*' OR age = ?) \n\t\tAND (type = '*' OR type = ?) \n\t\tAND (gender = -1 OR gender = ?) \n\t\tAND (wild = -1 OR wild = ?) \n\t\tAND (surface = -1 OR surface = ?) \n\t\tAND (kingdom = -1 OR kingdom = ?) \n\t\tAND (tiletype = -1 OR tiletype = ?) \n\t\tAND (zonename = '*' OR zonename = ?) \n\t\tAND (starthour = -1 OR starthour >= ?) \n\t\tAND (endhour = -1 OR endhour <= ?) \n\t\tAND (minaltitude = -1 OR (minaltitude <= ?)) \n\t\tAND (maxaltitude = -1 OR (maxaltitude >= ?)) \n\t\tAND (fat = '*' OR fat = ?) \n\t\tAND (diseased = -1 OR diseased = ?) \n\t\tAND (isunique = -1 OR isunique = ?) \n\t\tAND (isfromkingdom = -1 OR isfromkingdom = ?) \n\t\tAND (ishumanoid = -1 OR ishumanoid = ?) \n\t\tAND (iszombie = -1 OR iszombie = ?) \n\t\tAND (isfromrift = -1 OR isfromrift = ?) \n\t\tAND (minslope = -1 OR (minslope >= ?)) \n\t\tAND (maxslope = -1 OR (maxslope <= ?)) \n\t\tAND (minxpos = -1 OR (minxpos >= ?)) \n\t\tAND (minypos = -1 OR (minypos <= ?)) \n\t\tAND (maxxpos = -1 OR (maxxpos >= ?)) \n\t\tAND (maxypos = -1 OR (maxypos <= ?)) \n\t\tAND (weather = '*' OR weather = ?) \n\t\tAND (windstrength = '*' OR windstrength = ?) \n\t\tAND (winddirection = '*' OR winddirection = ?) \n\t\tAND (season = '*' OR season = ?) \n\t\tAND (deityinfluence = '*' OR deityinfluence = ?) \n\t\tAND (neardeed = -1 OR neardeed = ?) \n\t\tAND (neartower = -1 OR neartower = ?) \n\t\tAND enabled = 1";
   static String lootRuleQuerySimulation = "SELECT id, loottable, maxloot, rulename FROM FriyaLootRules WHERE id = ?";

   public static LootSystem getInstance() {
      if (instance == null) {
         instance = new LootSystem();
      }

      return instance;
   }

   public void listen(BeforeDropListener listener) {
      this.beforeDropListeners.add(listener);
   }

   public int getListenerCount() {
      return this.beforeDropListeners.size();
   }

   boolean notifyLootListeners(LootResult lr) {
      boolean shouldDropLoot = true;

      for(Iterator var4 = this.beforeDropListeners.iterator(); var4.hasNext(); Stats.inc("mods.calls")) {
         BeforeDropListener ll = (BeforeDropListener)var4.next();
         if (ll != null && !ll.onBeforeDrop(lr)) {
            logger.log(Level.INFO, "LootListener " + ll.toString() + " is preventing dropping of loot");
            shouldDropLoot = false;
         }
      }

      if (!shouldDropLoot) {
         Stats.inc("mods.cancelled");
         Item[] items = lr.getItems();
         Item[] var7 = items;
         int var6 = items.length;

         for(int var5 = 0; var5 < var6; ++var5) {
            Item i = var7[var5];
            logger.log(Level.INFO, "Destroying: " + i);
            Items.destroyItem(i.getWurmId());
         }

         lr.setItems(new Item[0]);
      }

      return shouldDropLoot;
   }

   private LootSystem() {
      if (this.createTables()) {
         this.importItemTemplates();
      }

      this.initEnchants();
   }

   private void initEnchants() {
      this.enchantAbbreviations.put("aosp", (byte)17);
      this.enchantAbbreviations.put("botd", (byte)47);
      this.enchantAbbreviations.put("bt", (byte)45);
      this.enchantAbbreviations.put("coc", (byte)13);
      this.enchantAbbreviations.put("courier", (byte)20);
      this.enchantAbbreviations.put("dm", (byte)44);
      this.enchantAbbreviations.put("fa", (byte)14);
      this.enchantAbbreviations.put("fb", (byte)33);
      this.enchantAbbreviations.put("lt", (byte)26);
      this.enchantAbbreviations.put("litdark", (byte)50);
      this.enchantAbbreviations.put("litdeep", (byte)48);
      this.enchantAbbreviations.put("litwoods", (byte)49);
      this.enchantAbbreviations.put("ms", (byte)31);
      this.enchantAbbreviations.put("nimb", (byte)32);
      this.enchantAbbreviations.put("nolo", (byte)29);
      this.enchantAbbreviations.put("opulence", (byte)15);
      this.enchantAbbreviations.put("rt", (byte)18);
      this.enchantAbbreviations.put("venom", (byte)27);
      this.enchantAbbreviations.put("wa", (byte)46);
      this.enchantAbbreviations.put("woa", (byte)16);
      this.enchantAbbreviations.put("animalsdemise", (byte)11);
      this.enchantAbbreviations.put("dragonsdemise", (byte)12);
      this.enchantAbbreviations.put("humansdemise", (byte)9);
      this.enchantAbbreviations.put("selfhealersdemise", (byte)10);
      this.enchantAbbreviations.put("fosdemise", (byte)1);
      this.enchantAbbreviations.put("libilasdemise", (byte)4);
      this.enchantAbbreviations.put("vynorasdemise", (byte)3);
      this.enchantAbbreviations.put("magranonssdemise", (byte)2);
      this.enchantAbbreviations.put("foscounter", (byte)5);
      this.enchantAbbreviations.put("libilascounter", (byte)8);
      this.enchantAbbreviations.put("vynorascounter", (byte)7);
      this.enchantAbbreviations.put("magranonscounter", (byte)6);
   }

   private void importItemTemplates() {
      logger.log(Level.INFO, "For convenience, importing all Item Templates as loot-drops with a corresponding loot-id");
      Connection con = ModSupportDb.getModSupportDb();
      ItemTemplate[] itemTemplates = ItemTemplateFactory.getInstance().getMostMaintenanceUpdated();
      int[] excludedItemIds = this.getExcludedItems();

      try {
         ItemTemplate[] var9 = itemTemplates;
         int var8 = itemTemplates.length;

         for(int var7 = 0; var7 < var8; ++var7) {
            ItemTemplate tpl = var9[var7];
            int currentImportedId = tpl.getTemplateId();
            if (IntStream.of(excludedItemIds).anyMatch((x) -> {
               return x == tpl.getTemplateId();
            })) {
               logger.log(Level.INFO, " Skipping: " + tpl.getTemplateId() + ", // '" + tpl.getName() + "'");
            } else {
               logger.log(Level.INFO, "Importing: " + tpl.getTemplateId() + ", // '" + tpl.getName() + "'");
               Object ps;
               if (Mod.sqlLogging) {
                  ps = new LoggableStatement(con, lootItemInsert);
               } else {
                  ps = con.prepareStatement(lootItemInsert);
               }

               ((PreparedStatement)ps).setInt(1, currentImportedId);
               ((PreparedStatement)ps).setString(2, "" + tpl.getTemplateId());
               ((PreparedStatement)ps).setString(3, tpl.getName());
               ((PreparedStatement)ps).setInt(4, -1);
               ((PreparedStatement)ps).setFloat(5, 10.0F);
               ((PreparedStatement)ps).setFloat(6, 60.0F);
               ((PreparedStatement)ps).setByte(7, (byte)1);
               ((PreparedStatement)ps).setDouble(8, 25.0);
               ((PreparedStatement)ps).setString(9, "Secret Santa");
               ((PreparedStatement)ps).setLong(10, 172800L);
               ((PreparedStatement)ps).setFloat(11, 90.0F);
               if (Mod.sqlLogging) {
                  logger.log(Level.INFO, "Executing: " + ((LoggableStatement)ps).getQueryString());
               }

               ((PreparedStatement)ps).execute();
            }
         }

      } catch (SQLException var10) {
         logger.log(Level.SEVERE, "Failed to import item templates", var10);
         throw new RuntimeException(var10);
      }
   }

   public int[] getExcludedItems() {
      int[] a1 = this.concat(this.concat(adminItems, balanceItems), this.concat(functionalItems, fluidItems));
      return this.concat(a1, largeItems);
   }

   private int[] concat(int[] a, int[] b) {
      int aLen = a.length;
      int bLen = b.length;
      int[] c = new int[aLen + bLen];
      System.arraycopy(a, 0, c, 0, aLen);
      System.arraycopy(b, 0, c, aLen, bLen);
      return c;
   }

   public boolean deleteRuleAndItsLootTable(String ruleName) {
      logger.log(Level.FINE, "Deleting loot rule: " + ruleName);

      try {
         Connection con = ModSupportDb.getModSupportDb();
         String sql = "SELECT id, loottable FROM FriyaLootRules WHERE rulename = ?";
         PreparedStatement ps = con.prepareStatement(sql);
         ps.setString(1, ruleName);
         ResultSet rs = ps.executeQuery();
         HashSet<Integer> deleteTables = new HashSet();
         HashSet<Integer> deleteRules = new HashSet();

         while(rs.next()) {
            deleteTables.add(rs.getInt("loottable"));
            deleteRules.add(rs.getInt("id"));
         }

         rs.close();
         ps.close();
         Statement statement = con.createStatement();
         con.setAutoCommit(false);
         Iterator var10 = deleteRules.iterator();

         int t;
         while(var10.hasNext()) {
            t = (Integer)var10.next();
            logger.log(Level.INFO, "Deleting loot rule #" + t);
            statement.addBatch("DELETE FROM FriyaLootRules WHERE id = " + t);
         }

         var10 = deleteTables.iterator();

         while(var10.hasNext()) {
            t = (Integer)var10.next();
            logger.log(Level.INFO, "Deleting loot table #" + t);
            statement.addBatch("DELETE FROM FriyaLootTables WHERE tableid = " + t);
         }

         statement.executeBatch();
         con.commit();
         rs.close();
         ps.close();
         return true;
      } catch (SQLException var11) {
         logger.log(Level.SEVERE, "Failed to delete Loot Rules");
         throw new RuntimeException(var11);
      }
   }

   public static boolean deleteAllLootRules() {
      logger.log(Level.FINE, "Deleting all loot rules...");

      try {
         Connection con = ModSupportDb.getModSupportDb();
         Statement statement = con.createStatement();
         con.setAutoCommit(false);
         statement.addBatch("DELETE FROM FriyaLootRules");
         statement.addBatch("DELETE FROM FriyaLootTables");
         statement.addBatch("DELETE FROM FriyaLootItems");
         statement.executeBatch();
         con.commit();
         return true;
      } catch (SQLException var2) {
         logger.log(Level.SEVERE, "Failed to delete Loot Rules");
         throw new RuntimeException(var2);
      }
   }

   private boolean insertStatements(ArrayList<String> sqlStatements) {
      String currentSqlStatement = null;

      try {
         Connection con = ModSupportDb.getModSupportDb();
         Statement statement = con.createStatement();
         con.setAutoCommit(false);
         Iterator var6 = sqlStatements.iterator();

         while(var6.hasNext()) {
            String sql = (String)var6.next();
            logger.log(Level.FINE, "Adding statement: " + sql);
            statement.addBatch(sql);
         }

         statement.executeBatch();
         con.commit();
         return true;
      } catch (SQLException var7) {
         logger.log(Level.SEVERE, "Failed to run Loot Rules batch at statement: " + currentSqlStatement);
         throw new RuntimeException(var7);
      }
   }

   public boolean addLootRules(String sqlBatch) {
      ArrayList<String> sqlStatements = getStatementsFromBatch(sqlBatch);
      return this.insertStatements(sqlStatements);
   }

   public boolean hasLootRule(String ruleName) {
      Connection con = ModSupportDb.getModSupportDb();
      String sql = "SELECT COUNT(*) AS cnt FROM FriyaLootRules WHERE rulename = ?";

      int ret;
      try {
         PreparedStatement ps = con.prepareStatement(sql);
         ps.setString(1, ruleName);
         ResultSet rs = ps.executeQuery();
         if (!rs.next()) {
            ps.close();
            throw new RuntimeException("Could not query FriyaLootRules");
         }

         ret = rs.getInt(1);
         ps.close();
      } catch (SQLException var8) {
         logger.log(Level.SEVERE, "Not good", var8);
         throw new RuntimeException("Could not get from resulset");
      }

      logger.log(Level.FINE, "hasRule(" + ruleName + "): " + ret);
      return ret > 0;
   }

   public boolean addLootRule(LootRule lootRule, LootItem[] lootItems) {
      Connection con = ModSupportDb.getModSupportDb();
      int newLootTableId = this.getNewLootTableId();
      LootItem[] var9 = lootItems;
      int var8 = lootItems.length;

      for(int var7 = 0; var7 < var8; ++var7) {
         LootItem li = var9[var7];
         li.save(con);
         String sql = "INSERT INTO FriyaLootTables(tableid, lootid) VALUES(" + newLootTableId + ", " + li.getId() + ")";

         try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.execute();
            ps.close();
         } catch (SQLException var12) {
            logger.log(Level.SEVERE, "Could not insert new loot-table row", var12);
            throw new RuntimeException("Halting");
         }
      }

      lootRule.setLootTable(newLootTableId);
      int lootRuleId = lootRule.save(con);
      logger.log(Level.FINE, "New LootRule id: " + lootRuleId);
      return true;
   }

   private int getNewLootTableId() {
      Connection con = ModSupportDb.getModSupportDb();
      String sql = "SELECT MAX(tableid)+1 AS nextid FROM FriyaLootTables";

      try {
         PreparedStatement ps = con.prepareStatement(sql);
         ResultSet rs = ps.executeQuery();
         if (rs.next()) {
            int ret = rs.getInt(1);
            ps.close();
            return ret;
         } else {
            ps.close();
            throw new RuntimeException("Could not get new loot-table ID");
         }
      } catch (SQLException var6) {
         logger.log(Level.SEVERE, "Not good", var6);
         throw new RuntimeException("Could not get from resulset");
      }
   }

   public Item[] getLoot(Creature c) {
      if (!this.canDropLoot(c)) {
         return new Item[0];
      } else {
         LootSet ls = this.getLootSet(c);
         return ls.getLoot();
      }
   }

   LootResult getLootResult(Creature c) {
      LootResult lr = new LootResult();
      if (!Mod.simulationOnStartup && !this.canDropLoot(c)) {
         Stats.inc("killed.ignored");
      } else {
         LootSet ls = this.getLootSet(c);
         Item[] loot = ls.getLoot();
         lr.setLootCap(ls.maxNumLoot);
         lr.setLootRules(ls.getLootRules());
         lr.setItems(loot);
      }

      lr.setCreature(c);
      return lr;
   }

   public boolean canDropLoot(Creature c) {
      if (c.isSuiciding()) {
         logger.log(Level.FINE, "canDropLoot() No loot. Creature is suiciding");
         return false;
      } else if (!Mod.everythingDropsLoot && (c.getLatestAttackers() == null || c.getLatestAttackers().length == 0 || !this.hasHumanAttacker(c))) {
         logger.log(Level.FINE, "canDropLoot() No loot. Creature did not have any valid attacker");
         return false;
      } else if (!Mod.playersDropLoot && c instanceof Player) {
         logger.log(Level.FINE, "canDropLoot() No loot. Creature was a player");
         return false;
      } else {
         return true;
      }
   }

   private boolean hasHumanAttacker(Creature c) {
      long[] attackerIds = c.getLatestAttackers();
      if (attackerIds != null && attackerIds.length != 0) {
         long[] var7 = attackerIds;
         int var6 = attackerIds.length;

         for(int var5 = 0; var5 < var6; ++var5) {
            long id = var7[var5];
            if (Players.getInstance().getPlayerOrNull(id) != null) {
               return true;
            }
         }

         return false;
      } else {
         return false;
      }
   }

   private LootSet getLootSet(Creature c) {
      Connection con = ModSupportDb.getModSupportDb();

      try {
         Object ps;
         if (Mod.sqlLogging) {
            ps = new LoggableStatement(con, Mod.simulationOnStartup ? lootRuleQuerySimulation : lootRuleQuery);
         } else {
            ps = con.prepareStatement(Mod.simulationOnStartup ? lootRuleQuerySimulation : lootRuleQuery);
         }

         if (Mod.simulationOnStartup) {
            ((PreparedStatement)ps).setInt(1, Mod.simulateRule);
         } else {
            FocusZone fs = this.getFocusZone(c);
            int slope = (int)c.getMovementScheme().getTileSteepness(c.getTileX(), c.getTileY(), c.getLayer());
            String[] windInfo = this.getWindInfo();
            String season = this.getSeason();
            String weather = this.getWeather();
            String rulingDeity = this.getRulingDeity(c);
            ((PreparedStatement)ps).setString(1, c.getNameWithoutPrefixes().toLowerCase());
            ((PreparedStatement)ps).setString(2, c.getStatus().getAgeString());
            ((PreparedStatement)ps).setString(3, this.getTypeString(c));
            ((PreparedStatement)ps).setByte(4, c.getSex());
            ((PreparedStatement)ps).setInt(5, c.isBred() ? 0 : 1);
            ((PreparedStatement)ps).setInt(6, c.isOnSurface() ? 1 : 2);
            ((PreparedStatement)ps).setByte(7, Zones.getKingdom(c.getTileX(), c.getTileY()));
            ((PreparedStatement)ps).setInt(8, Tiles.decodeType(Zones.getMesh(c.isOnSurface()).getTile(c.getTileX(), c.getTileY())));
            ((PreparedStatement)ps).setString(9, fs != null ? fs.getName() : "*");
            ((PreparedStatement)ps).setInt(10, WurmCalendar.getHour());
            ((PreparedStatement)ps).setInt(11, WurmCalendar.getHour());
            ((PreparedStatement)ps).setInt(12, c.getPosZDirts());
            ((PreparedStatement)ps).setInt(13, c.getPosZDirts());
            ((PreparedStatement)ps).setString(14, this.getFatString(c));
            ((PreparedStatement)ps).setInt(15, c.getDisease() == 0 ? 0 : 1);
            ((PreparedStatement)ps).setInt(16, c.isUnique() ? 1 : 0);
            ((PreparedStatement)ps).setInt(17, c.getKingdomId());
            ((PreparedStatement)ps).setInt(18, c.isHuman() ? 1 : 0);
            ((PreparedStatement)ps).setInt(19, c.isReborn() ? 1 : 0);
            ((PreparedStatement)ps).setInt(20, c.getTemplate().isRiftCreature() ? 1 : 0);
            ((PreparedStatement)ps).setInt(21, slope);
            ((PreparedStatement)ps).setInt(22, slope);
            ((PreparedStatement)ps).setInt(23, c.getTileX());
            ((PreparedStatement)ps).setInt(24, c.getTileY());
            ((PreparedStatement)ps).setInt(25, c.getTileX());
            ((PreparedStatement)ps).setInt(26, c.getTileY());
            ((PreparedStatement)ps).setString(27, weather);
            ((PreparedStatement)ps).setString(28, windInfo[0]);
            ((PreparedStatement)ps).setString(29, windInfo[1]);
            ((PreparedStatement)ps).setString(30, season);
            ((PreparedStatement)ps).setString(31, rulingDeity);
            ((PreparedStatement)ps).setInt(32, -1);
            ((PreparedStatement)ps).setInt(33, -1);
         }

         if (Mod.sqlLogging) {
            logger.log(Level.INFO, "Executing: " + ((LoggableStatement)ps).getQueryString());
         }

         ResultSet rs = ((PreparedStatement)ps).executeQuery();
         Stats.inc("query.total.lootrules");
         int maxNumLoot = 99;
         HashSet<Integer> tableIds = new HashSet();
         List<LootRule> rules = new ArrayList();

         while(rs.next()) {
            if (rs.getInt(3) == 0) {
               logger.log(Level.INFO, "Found a LootRule with maxNumLoot set to 0, skipping it");
               Stats.inc("rule.cancelled." + rs.getInt(1) + "." + rs.getString(4));
            } else {
               rules.add(new LootRule(rs.getInt(1), rs.getString(4), rs.getInt(2), rs.getByte(3)));
               tableIds.add(rs.getInt(2));
               if (rs.getInt(3) < maxNumLoot) {
                  maxNumLoot = rs.getInt(3);
               }

               Stats.inc("rule.triggered." + rs.getInt(1) + "." + rs.getString(4));
            }
         }

         ((PreparedStatement)ps).close();
         LootSet lootSet = new LootSet(tableIds, rules, maxNumLoot);
         return lootSet;
      } catch (SQLException var12) {
         logger.log(Level.SEVERE, "Failed to get matching creatures", var12);
         throw new RuntimeException(var12);
      }
   }

   private FocusZone getFocusZone(Creature c) {
      FocusZone fs = null;
      Iterator var4 = FocusZone.getZonesAt(c.getTileX(), c.getTileY()).iterator();
      if (var4.hasNext()) {
         FocusZone z = (FocusZone)var4.next();
         fs = z;
      }

      return fs;
   }

   private String getRulingDeity(Creature c) {
      String rulingDeity = "";

      try {
         FaithZone fz = Zones.getFaithZone(c.getTileX(), c.getTileY(), c.isOnSurface());
         if (fz != null) {
            rulingDeity = fz.currentRuler.name.toLowerCase();
         }
      } catch (NoSuchZoneException var4) {
      }

      return rulingDeity;
   }

   private String getWeather() {
      String weather;
      if ((double)Server.getWeather().getFog() > 0.5) {
         weather = "fog";
      } else if ((double)Server.getWeather().getRain() > 0.5) {
         weather = "precipitation";
      } else if ((double)Server.getWeather().getCloudiness() > 0.5) {
         weather = "overcast";
      } else {
         weather = "clear";
      }

      return weather;
   }

   private String getSeason() {
      String season;
      if (WurmCalendar.isSpring()) {
         season = "spring";
      } else if (WurmCalendar.isSummer()) {
         season = "summer";
      } else if (WurmCalendar.isAutumn()) {
         season = "autumn";
      } else {
         season = "winter";
      }

      return season;
   }

   private String[] getWindInfo() {
      String str = Server.getWeather().getWeatherString(false).substring(2).replace(".", "");
      return str.split(" is coming from the ");
   }

   private String getTypeString(Creature c) {
      String pre = c.getPrefixes();
      if (c.isUnique()) {
         pre = pre.substring(4);
      }

      String[] parts = pre.trim().split(" ", 3);
      if (parts.length == 3) {
         return parts[2];
      } else {
         return parts.length == 2 ? parts[1] : "";
      }
   }

   private String getFatString(Creature c) {
      String pre = c.getPrefixes();
      if (c.isUnique()) {
         pre = pre.substring(4);
      }

      String[] parts = pre.trim().split(" ", 3);
      return parts.length <= 1 || !parts[1].equals("starving") && !parts[1].equals("fat") ? "" : parts[1];
   }

   private boolean createTables() {
      Connection con = ModSupportDb.getModSupportDb();
      PreparedStatement ps = null;
      boolean createdTable = false;

      try {
         String sql;
         if (!ModSupportDb.hasTable(con, "FriyaLootRules")) {
            sql = "CREATE TABLE FriyaLootRules(\t\tid\t\t\t\t\tINTEGER\t\t\tPRIMARY KEY AUTOINCREMENT,\t\t\trulename\t\t\tVARCHAR(50)\t\tNULL,\t\t\t\t\t\t\tloottable\t\t\tINTEGER\t\t\tNOT NULL,\t\t\t\t\t\tmaxloot\t\t\t\tTINYINT\t\t\tNOT NULL DEFAULT 1,\t\t\t\tcreature\t\t\tVARCHAR(50)\t\tNOT NULL DEFAULT '*',\t\t\tage\t\t\t\t\tVARCHAR(20)\t\tNOT NULL DEFAULT '*',\t\t\ttype\t\t\t\tVARCHAR(20)\t\tNOT NULL DEFAULT '*',\t\t\tgender\t\t\t\tTINYINT\t\t\tNOT NULL DEFAULT -1,\t\t\twild\t\t\t\tTINYINT\t\t\tNOT NULL DEFAULT 1,\t\t\t\tsurface\t\t\t\tTINYINT\t\t\tNOT NULL DEFAULT -1,\t\t\tkingdom\t\t\t\tTINYINT\t\t\tNOT NULL DEFAULT -1,\t\t\ttiletype\t\t\tINTEGER\t\t\tNOT NULL DEFAULT -1,\t\t\tzonename\t\t\tVARCHAR(50)\t\tNOT NULL DEFAULT '*',\t\t\tstarthour\t\t\tTINYINT\t\t\tNOT NULL DEFAULT -1,\t\t\tendhour\t\t\t\tTINYINT\t\t\tNOT NULL DEFAULT -1,\t\t\tminaltitude\t\t\tINTEGER\t\t\tNOT NULL DEFAULT -1,\t\t\tmaxaltitude\t\t\tINTEGER\t\t\tNOT NULL DEFAULT -1,\t\t\tfat\t\t\t\t\tVARCHAR(20)\t\tNOT NULL DEFAULT '*',\t\t\tdiseased\t\t\tTINYINT\t\t\tNOT NULL DEFAULT -1,\t\t\tisunique\t\t\tTINYINT\t\t\tNOT NULL DEFAULT -1,\t\t\tisfromkingdom\t\tTINYINT\t\t\tNOT NULL DEFAULT -1,\t\t\tishumanoid\t\t\tTINYINT\t\t\tNOT NULL DEFAULT -1,\t\t\tiszombie\t\t\tTINYINT\t\t\tNOT NULL DEFAULT -1,\t\t\tisfromrift\t\t\tTINYINT\t\t\tNOT NULL DEFAULT -1,\t\t\tminslope\t\t\tINTEGER\t\t\tNOT NULL DEFAULT -1,\t\t\tmaxslope\t\t\tINTEGER\t\t\tNOT NULL DEFAULT -1,\t\t\tminxpos\t\t\t\tINTEGER\t\t\tNOT NULL DEFAULT -1,\t\t\tminypos\t\t\t\tINTEGER\t\t\tNOT NULL DEFAULT -1,\t\t\tmaxxpos\t\t\t\tINTEGER\t\t\tNOT NULL DEFAULT -1,\t\t\tmaxypos\t\t\t\tINTEGER\t\t\tNOT NULL DEFAULT -1,\t\t\tweather\t\t\t\tVARCHAR(20)\t\tNOT NULL DEFAULT '*',\t\t\twindstrength\t\tVARCHAR(20)\t\tNOT NULL DEFAULT '*',\t\t\twinddirection\t\tVARCHAR(20)\t\tNOT NULL DEFAULT '*',\t\t\tseason\t\t\t\tVARCHAR(20)\t\tNOT NULL DEFAULT '*',\t\t\tdeityinfluence\t\tVARCHAR(20)\t\tNOT NULL DEFAULT '*',\t\t\tneardeed\t\t\tTINYINT\t\t\tNOT NULL DEFAULT -1,\t\t\tneartower\t\t\tTINYINT\t\t\tNOT NULL DEFAULT -1,\t\t\tenabled\t\t\t\tTINYINT\t\t\tNOT NULL DEFAULT 1\t\t)";
            ps = con.prepareStatement(sql);
            ps.execute();
            createdTable = true;
            logger.log(Level.INFO, "Created FriyaLootRules");
         }

         if (!ModSupportDb.hasTable(con, "FriyaLootTables")) {
            sql = "CREATE TABLE FriyaLootTables(\t\ttableid\t\t\t\tINTEGER\t\t\tNOT NULL,\t\tlootid\t\t\t\tINTEGER\t\t\tNOT NULL)";
            ps = con.prepareStatement(sql);
            ps.execute();
            createdTable = true;
            logger.log(Level.INFO, "Created FriyaLootTables");
         }

         if (!ModSupportDb.hasTable(con, "FriyaLootItems")) {
            sql = "CREATE TABLE FriyaLootItems(\t\tid\t\t\t\t\tINTEGER\t\t\tPRIMARY KEY AUTOINCREMENT,\t\titemids\t\t\t\tVARCHAR(255)\tNOT NULL,\t\t\t\t\t\tname\t\t\t\tVARCHAR(50)\t\tNOT NULL DEFAULT '',\t\t\tmaterial\t\t\tTINYINT\t\t\tNOT NULL DEFAULT -1,\t\t\tstartql\t\t\t\tFLOAT\t\t\tNOT NULL DEFAULT 10,\t\t\tendql\t\t\t\tFLOAT\t\t\tNOT NULL DEFAULT 40,\t\t\tcanberare\t\t\tTINYINT\t\t\tNOT NULL DEFAULT 1,\t\t\t\tdropchance\t\t\tREAL\t\t\tNOT NULL DEFAULT 1,\t\t\t\tcreator\t\t\t\tVARCHAR(50)\t\tNULL,\t\t\t\t\t\t\tauxdata\t\t\t\tINTEGER\t\t\tNULL,\t\t\t\t\t\t\tdecaytime\t\t\tBIGINT\t\t\tNULL,\t\t\t\t\t\t\tdamage\t\t\t\tFLOAT\t\t\tNOT NULL DEFAULT 0,\t\t\t\tclonecount\t\t\tINTEGER\t\t\tNOT NULL DEFAULT 1,\t\t\t\tclonecountrandom\tINTEGER\t\t\tNOT NULL DEFAULT 0,\t\t\t\tenchants\t\t\tVARCHAR(255)\tNULL,\t\t\t\t\t\t\tenchantstrength\t\tINTEGER\t\t\tNOT NULL DEFAULT 0,\t\t\t\tenchantstrengthrandom\tINTEGER\t\tNOT NULL DEFAULT 0,\t\t\t\tcustommethod\t\tVARCHAR(255)\tNULL,\t\t\t\t\t\t\tcustomargument\t\tVARCHAR(255)\tNULL\t\t\t\t\t)";
            ps = con.prepareStatement(sql);
            ps.execute();
            createdTable = true;
            logger.log(Level.INFO, "Created FriyaLootItems");
         }

         this.upgradeTables(con, ps);
         return createdTable;
      } catch (SQLException var6) {
         logger.log(Level.SEVERE, "Failed to create tables", var6);
         throw new RuntimeException(var6);
      }
   }

   private void upgradeTables(Connection con, PreparedStatement ps) {
      try {
         if (!this.columnExists(con, "FriyaLootItems", "clonecount")) {
            logger.info("Upgrading FriyaLootItems with 'clonecount' support...");
            ps = con.prepareStatement("ALTER TABLE FriyaLootItems ADD COLUMN clonecount INTEGER NOT NULL DEFAULT 1");
            ps.execute();
            ps.close();
            ps = con.prepareStatement("ALTER TABLE FriyaLootItems ADD COLUMN clonecountrandom INTEGER NOT NULL DEFAULT 0");
            ps.execute();
            ps.close();
         }

         if (!this.columnExists(con, "FriyaLootItems", "enchants")) {
            logger.info("Upgrading FriyaLootItems with 'enchants' support...");
            ps = con.prepareStatement("ALTER TABLE FriyaLootItems ADD COLUMN enchants VARCHAR(255) NULL");
            ps.execute();
            ps.close();
            ps = con.prepareStatement("ALTER TABLE FriyaLootItems ADD COLUMN enchantstrength INTEGER NOT NULL DEFAULT 0");
            ps.execute();
            ps.close();
            ps = con.prepareStatement("ALTER TABLE FriyaLootItems ADD COLUMN enchantstrengthrandom INTEGER NOT NULL DEFAULT 0");
            ps.execute();
            ps.close();
         }

         if (!this.columnExists(con, "FriyaLootRules", "enabled")) {
            logger.info("Upgrading FriyaLootRules with 'enabled' support...");
            ps = con.prepareStatement("ALTER TABLE FriyaLootRules ADD COLUMN enabled TINYINT NOT NULL DEFAULT 1");
            ps.execute();
            ps.close();
         }

         if (!this.columnExists(con, "FriyaLootItems", "realtemplate")) {
            logger.info("Upgrading FriyaLootItems with 'realtemplate' support...");
            ps = con.prepareStatement("ALTER TABLE FriyaLootItems ADD COLUMN realtemplate INTEGER NOT NULL DEFAULT 0");
            ps.execute();
            ps.close();
         }

         if (!this.columnExists(con, "FriyaLootItems", "colorr")) {
            logger.info("Upgrading FriyaLootItems with 'color', 'weight' and 'forcerare' support...");
            ps = con.prepareStatement("ALTER TABLE FriyaLootItems ADD COLUMN colorr INTEGER NOT NULL DEFAULT -1");
            ps.execute();
            ps.close();
            ps = con.prepareStatement("ALTER TABLE FriyaLootItems ADD COLUMN colorg INTEGER NOT NULL DEFAULT -1");
            ps.execute();
            ps.close();
            ps = con.prepareStatement("ALTER TABLE FriyaLootItems ADD COLUMN colorb INTEGER NOT NULL DEFAULT -1");
            ps.execute();
            ps.close();
            ps = con.prepareStatement("ALTER TABLE FriyaLootItems ADD COLUMN colorrrandom INTEGER NOT NULL DEFAULT 0");
            ps.execute();
            ps.close();
            ps = con.prepareStatement("ALTER TABLE FriyaLootItems ADD COLUMN colorgrandom INTEGER NOT NULL DEFAULT 0");
            ps.execute();
            ps.close();
            ps = con.prepareStatement("ALTER TABLE FriyaLootItems ADD COLUMN colorbrandom INTEGER NOT NULL DEFAULT 0");
            ps.execute();
            ps.close();
            ps = con.prepareStatement("ALTER TABLE FriyaLootItems ADD COLUMN weight INTEGER NOT NULL DEFAULT -1");
            ps.execute();
            ps.close();
            ps = con.prepareStatement("ALTER TABLE FriyaLootItems ADD COLUMN weightrandom INTEGER NOT NULL DEFAULT 0");
            ps.execute();
            ps.close();
            ps = con.prepareStatement("ALTER TABLE FriyaLootItems ADD COLUMN forcerare INTEGER NOT NULL DEFAULT 0");
            ps.execute();
            ps.close();
         }

      } catch (SQLException var4) {
         logger.log(Level.SEVERE, "Failed to upgrade tables", var4);
         throw new RuntimeException(var4);
      }
   }

   private boolean columnExists(Connection con, String table, String column) {
      boolean found = false;
      ResultSet rs = null;
      PreparedStatement ps = null;

      try {
         ps = con.prepareStatement("PRAGMA table_info(" + table + ")");
         rs = ps.executeQuery();

         while(rs.next()) {
            if (rs.getString("name").equals(column)) {
               found = true;
               break;
            }
         }
      } catch (SQLException var16) {
         logger.log(Level.SEVERE, "Could not determine whether a column existed in a table, this might cause problems later on....", var16);
      } finally {
         try {
            rs.close();
            ps.close();
         } catch (SQLException var15) {
            var15.printStackTrace();
         }

      }

      return found;
   }

   public static ArrayList<String> getStatementsFromBatch(String sqlBatch) {
      ArrayList<String> ret = new ArrayList();
      Scanner s = new Scanner(sqlBatch);
      s.useDelimiter("/\\*[\\s\\S]*?\\*/|--[^\\r\\n]*");

      try {
         StringBuffer currentStatement = new StringBuffer();

         while(true) {
            String line;
            do {
               do {
                  if (!s.hasNext()) {
                     return ret;
                  }

                  line = s.next();
                  if (line.startsWith("/*!") && line.endsWith("*/")) {
                     int i = line.indexOf(32);
                     line = line.substring(i + 1, line.length() - " */".length());
                  }
               } while(line.trim().length() <= 0);

               currentStatement.append(line);
            } while(!line.contains(";"));

            String[] tmp = currentStatement.toString().split(";");
            String[] var9 = tmp;
            int var8 = tmp.length;

            for(int var7 = 0; var7 < var8; ++var7) {
               String ln = var9[var7];
               if (ln.trim().length() != 0) {
                  ret.add(ln);
               }
            }

            currentStatement.setLength(0);
         }
      } finally {
         s.close();
      }
   }
}
