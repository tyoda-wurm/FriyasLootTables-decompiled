package com.friya.wurmonline.server.loot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LootRule {
   private static Logger logger = Logger.getLogger(LootRule.class.getName());
   private int id;
   private String ruleName = "";
   private int lootTable = -1;
   private byte maxLoot = 1;
   private String creature = "*";
   private String age = "*";
   private String type = "*";
   private byte gender = -1;
   private byte wild = -1;
   private byte surface = -1;
   private byte kingdom = -1;
   private int tileType = -1;
   private String zoneName = "*";
   private byte startHour = -1;
   private byte endHour = -1;
   private int minAltitude = -1;
   private int maxAltitude = -1;
   private String fat = "*";
   private byte diseased = -1;
   private byte isUnique = -1;
   private byte isFromKingdom = -1;
   private byte isHumanoid = -1;
   private byte isZombie = -1;
   private byte isFromRift = -1;
   private int minSlope = -1;
   private int maxSlope = -1;
   private int minXpos = -1;
   private int minYpos = -1;
   private int maxXpos = -1;
   private int maxYpos = -1;
   private String weather = "*";
   private String windStrength = "*";
   private String windDirection = "*";
   private String season = "*";
   private String deityInfluence = "*";
   private byte nearDeed = -1;
   private byte nearTower = -1;

   public LootRule(String ruleName) {
      this.ruleName = ruleName;
   }

   public LootRule(int id, String ruleName, int lootTable, byte maxLoot) {
      this.id = id;
      this.lootTable = lootTable;
      this.maxLoot = maxLoot;
      this.ruleName = ruleName;
   }

   int save(Connection con) {
      boolean update = false;
      String sql;
      if (this.id > 0) {
         sql = "UPDATE FriyaLootRules SET rulename=?, loottable=?, maxloot=?, creature=?, age=?, type=?, gender=?, wild=?, surface=?, kingdom=?, tiletype=?, zonename=?, starthour=?, endhour=?,minaltitude=?, maxaltitude=?, fat=?, diseased=?, isunique=?, isfromkingdom=?, ishumanoid=?, iszombie=?, isfromrift=?, minslope=?, maxslope=?,minxpos=?, minypos=?, maxxpos=?, maxypos=?, weather=?, windstrength=?, winddirection=?, season=?, deityinfluence=?, neardeed=?, neartower=? WHERE id = " + this.getId();
         update = true;
      } else {
         sql = "INSERT INTO FriyaLootRules(rulename, loottable, maxloot, creature, age, type, gender, wild, surface, kingdom, tiletype, zonename, starthour, endhour, minaltitude, maxaltitude, fat, diseased, isunique, isfromkingdom, ishumanoid, iszombie, isfromrift, minslope, maxslope, minxpos, minypos, maxxpos, maxypos, weather, windstrength, winddirection, season, deityinfluence, neardeed, neartower) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      try {
         PreparedStatement ps = con.prepareStatement(sql, 1);
         int i = 1;
         ps.setString(i++, this.ruleName);
         ps.setInt(i++, this.lootTable);
         ps.setInt(i++, this.maxLoot);
         ps.setString(i++, this.creature);
         ps.setString(i++, this.age);
         ps.setString(i++, this.type);
         ps.setByte(i++, this.gender);
         ps.setByte(i++, this.wild);
         ps.setByte(i++, this.surface);
         ps.setByte(i++, this.kingdom);
         ps.setInt(i++, this.tileType);
         ps.setString(i++, this.zoneName);
         ps.setByte(i++, this.startHour);
         ps.setByte(i++, this.endHour);
         ps.setInt(i++, this.minAltitude);
         ps.setInt(i++, this.maxAltitude);
         ps.setString(i++, this.fat);
         ps.setByte(i++, this.diseased);
         ps.setByte(i++, this.isUnique);
         ps.setByte(i++, this.isFromKingdom);
         ps.setByte(i++, this.isHumanoid);
         ps.setByte(i++, this.isZombie);
         ps.setByte(i++, this.isFromRift);
         ps.setInt(i++, this.minSlope);
         ps.setInt(i++, this.maxSlope);
         ps.setInt(i++, this.minXpos);
         ps.setInt(i++, this.minYpos);
         ps.setInt(i++, this.maxXpos);
         ps.setInt(i++, this.maxYpos);
         ps.setString(i++, this.weather);
         ps.setString(i++, this.windStrength);
         ps.setString(i++, this.windDirection);
         ps.setString(i++, this.season);
         ps.setString(i++, this.deityInfluence);
         ps.setByte(i++, this.nearDeed);
         ps.setByte(i++, this.nearTower);
         if (update) {
            ps.executeUpdate();
         } else {
            ps.execute();
         }

         ResultSet rs = ps.getGeneratedKeys();
         if (rs != null) {
            rs.next();
            this.id = rs.getInt(1);
            logger.log(Level.FINE, "Inserted item as: " + this.id);
         } else {
            logger.log(Level.WARNING, "no resultset back from getGeneratedKeys(), probably means nothing was created!");
         }

         ps.close();
      } catch (SQLException var7) {
         logger.log(Level.SEVERE, "Failed to save LootItem", var7);
         throw new RuntimeException(var7);
      }

      return this.id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public int getId() {
      return this.id;
   }

   public void setRuleName(String ruleName) {
      this.ruleName = ruleName;
   }

   public String getRuleName() {
      return this.ruleName;
   }

   public void setLootTable(int lootTable) {
      this.lootTable = lootTable;
   }

   public int getLootTable() {
      return this.lootTable;
   }

   public void setMaxLoot(byte maxLoot) {
      this.maxLoot = maxLoot;
   }

   public byte getMaxLoot() {
      return this.maxLoot;
   }

   public void setCreature(String creature) {
      this.creature = creature;
   }

   public String getCreature() {
      return this.creature;
   }

   public void setAge(String age) {
      this.age = age;
   }

   public String getAge() {
      return this.age;
   }

   public void setType(String type) {
      this.type = type;
   }

   public String getType() {
      return this.type;
   }

   public void setGender(byte gender) {
      this.gender = gender;
   }

   public byte getGender() {
      return this.gender;
   }

   public void setWild(byte wild) {
      this.wild = wild;
   }

   public byte getWild() {
      return this.wild;
   }

   public void setSurface(byte surface) {
      this.surface = surface;
   }

   public byte getSurface() {
      return this.surface;
   }

   public void setKingdom(byte kingdom) {
      this.kingdom = kingdom;
   }

   public byte getKingdom() {
      return this.kingdom;
   }

   public void setTileType(int tileType) {
      this.tileType = tileType;
   }

   public int getTileType() {
      return this.tileType;
   }

   public void setZoneName(String zoneName) {
      this.zoneName = zoneName;
   }

   public String getZoneName() {
      return this.zoneName;
   }

   public void setStartHour(byte startHour) {
      this.startHour = startHour;
   }

   public byte getStartHour() {
      return this.startHour;
   }

   public void setEndHour(byte endHour) {
      this.endHour = endHour;
   }

   public byte getEndHour() {
      return this.endHour;
   }

   public void setMinAltitude(int minAltitude) {
      this.minAltitude = minAltitude;
   }

   public int getMinAltitude() {
      return this.minAltitude;
   }

   public void setMaxAltitude(int maxAltitude) {
      this.maxAltitude = maxAltitude;
   }

   public int getMaxAltitude() {
      return this.maxAltitude;
   }

   public void setFat(String fat) {
      this.fat = fat;
   }

   public String getFat() {
      return this.fat;
   }

   public void setDiseased(byte diseased) {
      this.diseased = diseased;
   }

   public byte getDiseased() {
      return this.diseased;
   }

   public void setIsUnique(byte isUnique) {
      this.isUnique = isUnique;
   }

   public byte getIsUnique() {
      return this.isUnique;
   }

   public void setIsFromKingdom(byte isFromKingdom) {
      this.isFromKingdom = isFromKingdom;
   }

   public byte getIsFromKingdom() {
      return this.isFromKingdom;
   }

   public void setIsHumanoid(byte isHumanoid) {
      this.isHumanoid = isHumanoid;
   }

   public byte getIsHumanoid() {
      return this.isHumanoid;
   }

   public void setIsZombie(byte isZombie) {
      this.isZombie = isZombie;
   }

   public byte getIsZombie() {
      return this.isZombie;
   }

   public void setIsFromRift(byte isFromRift) {
      this.isFromRift = isFromRift;
   }

   public byte getIsFromRift() {
      return this.isFromRift;
   }

   public void setMinSlope(int minSlope) {
      this.minSlope = minSlope;
   }

   public int getMinSlope() {
      return this.minSlope;
   }

   public void setMaxSlope(int maxSlope) {
      this.maxSlope = maxSlope;
   }

   public int getMaxSlope() {
      return this.maxSlope;
   }

   public void setMinXpos(int minXpos) {
      this.minXpos = minXpos;
   }

   public int getMinXpos() {
      return this.minXpos;
   }

   public void setMinYpos(int minYpos) {
      this.minYpos = minYpos;
   }

   public int getMinYpos() {
      return this.minYpos;
   }

   void setMaxXpos(int maxXpos) {
      this.maxXpos = maxXpos;
   }

   int getMaxXpos() {
      return this.maxXpos;
   }

   public void setMaxYpos(int maxYpos) {
      this.maxYpos = maxYpos;
   }

   public int getMaxYpos() {
      return this.maxYpos;
   }

   void setWeather(String weather) {
      this.weather = weather;
   }

   String getWeather() {
      return this.weather;
   }

   public void setWindStrength(String windStrength) {
      this.windStrength = windStrength;
   }

   public String getWindStrength() {
      return this.windStrength;
   }

   public void setWindDirection(String windDirection) {
      this.windDirection = windDirection;
   }

   public String getWindDirection() {
      return this.windDirection;
   }

   public void setSeason(String season) {
      this.season = season;
   }

   public String getSeason() {
      return this.season;
   }

   public void setDeityInfluence(String deityInfluence) {
      this.deityInfluence = deityInfluence;
   }

   public String getDeityInfluence() {
      return this.deityInfluence;
   }

   public void setNearDeed(byte nearDeed) {
      this.nearDeed = nearDeed;
   }

   public byte getNearDeed() {
      return this.nearDeed;
   }

   public void setNearTower(byte nearTower) {
      this.nearTower = nearTower;
   }

   public byte getNearTower() {
      return this.nearTower;
   }
}
