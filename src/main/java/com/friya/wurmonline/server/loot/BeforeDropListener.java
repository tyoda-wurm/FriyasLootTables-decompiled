package com.friya.wurmonline.server.loot;

public interface BeforeDropListener {
   boolean onBeforeDrop(LootResult var1);
}
