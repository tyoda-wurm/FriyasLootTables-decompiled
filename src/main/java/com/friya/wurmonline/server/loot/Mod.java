package com.friya.wurmonline.server.loot;

import com.wurmonline.server.Items;
import com.wurmonline.server.creatures.Communicator;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javassist.CtClass;
import javassist.CtPrimitiveType;
import javassist.NotFoundException;
import javassist.bytecode.Descriptor;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.classhooks.InvocationHandlerFactory;
import org.gotti.wurmunlimited.modloader.interfaces.*;

public class Mod implements WurmServerMod, Initable, Configurable, ServerStartedListener, PlayerMessageListener, Versioned {
   public static final String version = "ty1.0";
   private static Logger logger = Logger.getLogger(Mod.class.getName());
   static boolean enabled = true;
   static boolean everythingDropsLoot = false;
   static boolean playersDropLoot = false;
   static boolean createDefaultRules = false;
   static boolean deleteAllRules = false;
   static boolean logExecutionCost = false;
   static boolean sqlLogging = false;
   static boolean simulationOnStartup = false;
   static int simulateRule = 8;
   static int simulateAmount = 10000;

   public void configure(Properties properties) {
      enabled = Boolean.valueOf(properties.getProperty("enabled", String.valueOf(enabled)));
      everythingDropsLoot = Boolean.valueOf(properties.getProperty("everythingDropsLoot", String.valueOf(everythingDropsLoot)));
      playersDropLoot = Boolean.valueOf(properties.getProperty("playersDropLoot", String.valueOf(playersDropLoot)));
      createDefaultRules = Boolean.valueOf(properties.getProperty("createDefaultRules", String.valueOf(createDefaultRules)));
      deleteAllRules = Boolean.valueOf(properties.getProperty("deleteAllRules", String.valueOf(deleteAllRules)));
      logExecutionCost = Boolean.valueOf(properties.getProperty("logExecutionCost", String.valueOf(logExecutionCost)));
      sqlLogging = Boolean.valueOf(properties.getProperty("sqlLogging", String.valueOf(sqlLogging)));
      logger.info("Loaded LootTable configuration");
   }

   public void init() {
      if (!enabled) {
         logger.log(Level.WARNING, "Loot tables are DISABLED through configuration flag 'enabled'");
      } else {
         Stats.reset();
         this.setupInterceptDie();
      }
   }

   @Override
   public void onServerStarted() {
      if (!enabled) {
         logger.log(Level.WARNING, "Loot tables are DISABLED through configuration flag 'enabled'");
      } else {
         if (simulationOnStartup) {
            logger.info("------------------------------------------------------------------------------------------------------");
            logger.info("                                      Starting simulation");
            logger.info("------------------------------------------------------------------------------------------------------");
            logger.info("Will trigger rule " + simulateRule + " exactly " + simulateAmount + " times");
            HashMap<String, Integer> dropAmount = new HashMap();

            for(int j = 0; j < simulateAmount; ++j) {
               LootSystem ls = LootSystem.getInstance();
               LootResult lr = ls.getLootResult((Creature)null);
               Item[] items = lr.getItems();
               Item[] var9 = items;
               int var8 = items.length;

               for(int var7 = 0; var7 < var8; ++var7) {
                  Item i = var9[var7];
                  String name = "template: " + i.getTemplateId() + " name: " + i.getName();
                  if (dropAmount.containsKey(name)) {
                     dropAmount.put(name, (Integer)dropAmount.get(name) + 1);
                  } else {
                     dropAmount.put(name, 1);
                  }

                  Items.destroyItem(i.getWurmId());
               }
            }

            Iterator it = dropAmount.entrySet().iterator();

            while(it.hasNext()) {
               Map.Entry<String, Integer> pair = (Map.Entry)it.next();
               logger.info((float)(Integer)pair.getValue() / (float)simulateAmount * 100.0F + "%\t" + (String)pair.getKey() + " dropped " + pair.getValue() + " times");
            }

            logger.info("------------------------------------------------------------------------------------------------------");
            logger.info("                                        Simulation done");
            logger.info("------------------------------------------------------------------------------------------------------");
            simulationOnStartup = false;
         }

         if (deleteAllRules) {
            LootSystem.deleteAllLootRules();
         }

         if (createDefaultRules) {
            createLootRules();
         }

      }
   }

   private void setupInterceptDie() {
      String descriptor;
      try {
         descriptor = Descriptor.ofMethod(CtPrimitiveType.voidType, new CtClass[]{CtPrimitiveType.booleanType, HookManager.getInstance().getClassPool().get("java.lang.String")});
      } catch (NotFoundException var3) {
         throw new RuntimeException("Failed to find String class -- this is hilarious!");
      }

      HookManager.getInstance().registerHook("com.wurmonline.server.creatures.Creature", "die", descriptor, new InvocationHandlerFactory() {
         public InvocationHandler createInvocationHandler() {
            return new InvocationHandler() {
               public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                  DecimalFormat df = null;
                  long start = 0L;
                  if (Mod.logExecutionCost) {
                     df = new DecimalFormat("#.#########");
                     start = System.nanoTime();
                  }

                  Mod.this.interceptDie(proxy, args);
                  if (Mod.logExecutionCost) {
                     Mod.logger.log(Level.INFO, "LootTables interception spent " + df.format((double)(System.nanoTime() - start) / 1.0E9) + "s (this number include calls to " + LootSystem.getInstance().getListenerCount() + " mods that hook into LootTables)");
                  }

                  Object result = method.invoke(proxy, args);
                  return result;
               }
            };
         }
      });
   }

   private void interceptDie(Object proxy, Object[] args) {
      try {
         Creature c = (Creature)proxy;
         if (c.isPlayer()) {
            Stats.inc("killed.players");
            Stats.inc("killed.player." + c.getName());
         } else {
            Stats.inc("killed.npcs");
            Stats.inc("killed.npc." + c.getTypeName());
         }

         LootSystem ls = LootSystem.getInstance();
         LootResult lr = ls.getLootResult(c);
         if (!ls.notifyLootListeners(lr)) {
            return;
         }

         Item[] items = lr.getItems();
         Item[] var10 = items;
         int var9 = items.length;

         for(int var8 = 0; var8 < var9; ++var8) {
            Item i = var10[var8];
            c.getInventory().insertItem(i, true);
         }

         Stats.outputPeriodically();
      } catch (Exception var11) {
         logger.log(Level.SEVERE, "some mod's onBeforeDrop() caused an exception, this is caught and thrown away to not prevent things from dying", var11);
         Stats.inc("mods.exceptions");
      }

   }

   @SuppressWarnings("deprecation")
   @Override
   public boolean onPlayerMessage(Communicator c, String msg) {
      boolean intercepted = false;
      if (c.getPlayer() != null && c.getPlayer().getPower() > 0 && msg.startsWith("/lootstats")) {
         intercepted = true;
         Stats.output(c, false);
      }

      return intercepted;
   }

   @Override
   public MessagePolicy onPlayerMessage(Communicator communicator, String message, String title) {
      return onPlayerMessage(communicator, message) ? MessagePolicy.DISCARD : MessagePolicy.PASS;
   }

   static void createLootRules() {
      LootSystem ls = LootSystem.getInstance();
      String ruleName = "[all NPCs] Rare stuff";
      ls.deleteRuleAndItsLootTable(ruleName);
      if (!ls.hasLootRule(ruleName)) {
         LootRule lr = new LootRule(ruleName);
         LootItem[] li = new LootItem[]{new LootItem("370,443,465,489,509,515,600,666,667,668,700,738,781,834,836,844,867,967,972,973,974,975,976,977,978,979,980,1014,1015,1032,1049,1050,1051,1052,1053,1054,1055,1056,1057,1058,1059,1060,1061,1062,1063,1064,1065,1066,1076,1077,1078,1079,1080,1081,1082,1083,1084,1085,1086,1087,1088,1089,1090,1092,1093,1094,1095,1099", 0.01)};
         ls.addLootRule(lr, li);
      }
   }

   @Override
   public String getVersion(){
      return version;
   }
}
