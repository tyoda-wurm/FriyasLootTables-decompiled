package com.friya.wurmonline.server.loot;

import java.util.HashMap;
import java.util.Iterator;

public class LootTable {
   private int tableId;
   boolean fetchedChildren = false;
   private HashMap<Integer, LootItem> lootItems = new HashMap();

   LootTable(int id) {
      this.setTableId(id);
   }

   void addLootItem(LootItem li) {
      this.lootItems.put(li.getId(), li);
   }

   public int getTableId() {
      return this.tableId;
   }

   private void setTableId(int tableId) {
      this.tableId = tableId;
   }

   public String toString() {
      return "LootTable#" + this.tableId;
   }

   public HashMap<Integer, LootItem> getLootItemCandidatesByWurmId() {
      HashMap<Integer, LootItem> wurmIdLootItems = new HashMap();
      Iterator var3 = this.lootItems.values().iterator();

      while(var3.hasNext()) {
         LootItem li = (LootItem)var3.next();
         int[] wurmItemIds = li.getWurmItemIds();

         for(int x = 0; x < wurmItemIds.length; ++x) {
            wurmIdLootItems.put(wurmItemIds[x], li);
         }
      }

      return wurmIdLootItems;
   }
}
