package com.friya.wurmonline.server.loot;

import com.wurmonline.server.Players;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.creatures.Creatures;
import com.wurmonline.server.items.Item;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LootResult {
   private int maxNumberOfLoot = 0;
   private List<LootRule> lootRules = new ArrayList();
   private Item[] items;
   private Creature creature;
   private Creature[] attackerObs;
   private boolean fetchedAttackers = false;
   private float creatureStrength = 0.0F;

   LootResult() {
   }

   public List<LootRule> getLootRules() {
      return this.lootRules;
   }

   void setLootRules(List<LootRule> lootRules) {
      this.lootRules = lootRules;
   }

   public Item[] getItems() {
      if (this.items == null) {
         this.items = new Item[0];
      }

      return this.items;
   }

   void setItems(Item[] items) {
      this.items = items;
   }

   public Creature getCreature() {
      return this.creature;
   }

   void setCreature(Creature creature) {
      if (!Mod.simulationOnStartup) {
         this.creature = creature;
         this.creatureStrength = creature.getBaseCombatRating() + creature.getBonusCombatRating();
         this.setKillers();
      }
   }

   private void setKillers() {
      List<Creature> tmpAttackers = new ArrayList();
      long[] rawAttackerIds = this.creature.getLatestAttackers();
      long[] var8 = rawAttackerIds;
      int var7 = rawAttackerIds.length;

      for(int var6 = 0; var6 < var7; ++var6) {
         long a = var8[var6];
         Creature c = Players.getInstance().getPlayerOrNull(a);
         if (c == null) {
            c = Creatures.getInstance().getCreatureOrNull(a);
         }

         if (c != null) {
            tmpAttackers.add(c);
         }
      }

      this.attackerObs = (Creature[])tmpAttackers.toArray(new Creature[0]);
      this.fetchedAttackers = true;
   }

   public boolean isKilledByPlayer() {
      Creature[] attackers = this.getKillers();
      Creature[] var5 = attackers;
      int var4 = attackers.length;

      for(int var3 = 0; var3 < var4; ++var3) {
         Creature c = var5[var3];
         if (Players.getInstance().getPlayerOrNull(c.getWurmId()) != null) {
            return true;
         }
      }

      return false;
   }

   public Creature[] getKillers() {
      if (!this.fetchedAttackers) {
         this.setKillers();
      }

      return this.attackerObs;
   }

   public LootRule getLootRule(String ruleName) {
      Iterator var3 = this.lootRules.iterator();

      while(var3.hasNext()) {
         LootRule lr = (LootRule)var3.next();
         if (lr.getRuleName().equals(ruleName)) {
            return lr;
         }
      }

      return null;
   }

   public boolean hasLootRule(String ruleName) {
      return this.getLootRule(ruleName) != null;
   }

   public int getLootCap() {
      return this.maxNumberOfLoot;
   }

   void setLootCap(int maxNumberOfLoot) {
      this.maxNumberOfLoot = maxNumberOfLoot;
   }

   public float getCreatureStrength() {
      return this.creatureStrength;
   }

   public void setCreatureStrength(float creatureStrength) {
      this.creatureStrength = creatureStrength;
   }
}
