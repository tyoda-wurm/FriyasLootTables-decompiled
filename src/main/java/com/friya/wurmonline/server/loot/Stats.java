package com.friya.wurmonline.server.loot;

import com.wurmonline.server.Players;
import com.wurmonline.server.creatures.Communicator;
import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Stats {
   private static Logger logger = Logger.getLogger(Stats.class.getName());
   static long startTime = 0L;
   static long lastLogOutput = 0L;
   static Map<String, Long> miscStats = new TreeMap();
   private static final DateTimeFormatter compactDateTimeFormatter = DateTimeFormatter.ofPattern("ddMMMyyyy HH:mm");

   public Stats() {
   }

   static void outputPeriodically() {
      if (lastLogOutput + 3600000L < System.currentTimeMillis()) {
         lastLogOutput = System.currentTimeMillis();
         output((Communicator)null, true);
      }

   }

   static void inc(String statName) {
      if (!miscStats.containsKey(statName)) {
         miscStats.put(statName, 0L);
      }

      miscStats.put(statName, (Long)miscStats.get(statName) + 1L);
   }

   static void inc(String statName, int count) {
      if (!miscStats.containsKey(statName)) {
         miscStats.put(statName, 0L);
      }

      miscStats.put(statName, (Long)miscStats.get(statName) + (long)count);
   }

   static void set(String statName, long count) {
      miscStats.put(statName, count);
   }

   static void reset() {
      miscStats.clear();
      startTime = System.currentTimeMillis();
   }

   static void output(Communicator c, boolean logOnly) {
      StringBuffer msgs = new StringBuffer();
      msgs.append("==== LootTable statistics at " + millisecondsToCompactDateTime(System.currentTimeMillis()) + ". Gathering started at " + millisecondsToCompactDateTime(startTime) + " ====\n");
      set("misc.players.online", (long)Players.getInstance().getNumberOfPlayers());
      Iterator var4 = miscStats.entrySet().iterator();

      while(var4.hasNext()) {
         Map.Entry<String, Long> entry = (Map.Entry)var4.next();
         msgs.append("    " + (String)entry.getKey() + " = " + entry.getValue() + "\n");
      }

      msgs.append("==== End of LootTable statistics ====\n");
      if (!logOnly) {
         String[] lines = msgs.toString().split("\\n");

         for(int i = 0; i < lines.length; ++i) {
            if (i % 2 == 0) {
               c.sendNormalServerMessage(lines[i]);
            } else {
               c.sendSafeServerMessage(lines[i]);
            }
         }
      }

      logger.log(Level.INFO, "\n" + msgs.toString());
   }

   static String millisecondsToCompactDateTime(long ms) {
      return (new Timestamp(ms)).toLocalDateTime().format(compactDateTimeFormatter);
   }
}
